import { config } from "dotenv";
import consola from "consola";
import app from "./src/app";

config();

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  consola.info(`Server started on port ${PORT}`);
});
