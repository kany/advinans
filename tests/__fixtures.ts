export const sparWsdlResponse = `
<?xml version='1.0' encoding='UTF-8'?>
<!-- Published by JAX-WS RI (https://github.com/eclipse-ee4j/metro-jax-ws). RI's version is JAX-WS RI 2.3.6 git-revision#d201abe. -->
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://statenspersonadressregister.se/personsok/2021.1" xmlns:fraga="http://statenspersonadressregister.se/schema/personsok/2021.1/personsokningfraga" xmlns:svar="http://statenspersonadressregister.se/schema/personsok/2021.1/personsokningsvar" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="PersonsokService" targetNamespace="http://statenspersonadressregister.se/personsok/2021.1">
    <wsdl:types>
        <xsd:schema targetNamespace="http://statenspersonadressregister.se/schema/personsok/2021.1/personsokningfraga">
            <xsd:include schemaLocation="http://xmls.statenspersonadressregister.se/se/spar/granssnitt/personsok/2021.1/PersonsokningFraga.xsd"/>
        </xsd:schema>
        <xsd:schema targetNamespace="http://statenspersonadressregister.se/schema/personsok/2021.1/personsokningsvar">
            <xsd:include schemaLocation="http://xmls.statenspersonadressregister.se/se/spar/granssnitt/personsok/2021.1/PersonsokningSvar.xsd"/>
        </xsd:schema>
    </wsdl:types>
    <wsdl:message name="PersonsokRequest">
        <wsdl:part element="fraga:SPARPersonsokningFraga" name="parameters"/>
    </wsdl:message>
    <wsdl:message name="PersonsokResponse">
        <wsdl:part element="svar:SPARPersonsokningSvar" name="parameters"/>
    </wsdl:message>
    <wsdl:portType name="PersonsokService">
        <wsdl:operation name="PersonSok">
            <wsdl:input message="tns:PersonsokRequest"/>
            <wsdl:output message="tns:PersonsokResponse"/>
        </wsdl:operation>
    </wsdl:portType>
    <wsdl:binding name="PersonsokServiceSOAP" type="tns:PersonsokService">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
        <wsdl:operation name="PersonSok">
            <soap:operation soapAction="http://skatteverket.se/spar/personsok/2021.1/PersonsokService/Personsok"/>
            <wsdl:input>
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal"/>
            </wsdl:output>
        </wsdl:operation>
    </wsdl:binding>
    <wsdl:service name="PersonsokService">
        <wsdl:port binding="tns:PersonsokServiceSOAP" name="PersonsokServiceSOAP">
            <soap:address location="http://test-personsok.statenspersonadressregister.se/"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>`;

export const sparPersonSokDto = `
<?xml version='1.0' encoding='UTF-8'?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns25:SPARPersonsokningSvar xmlns="http://statenspersonadressregister.se/schema/komponent/metadata/identifieringsinformationWs-1.1" xmlns:ns2="http://statenspersonadressregister.se/schema/komponent/generellt/datumtid-1.1" xmlns:ns3="http://statenspersonadressregister.se/schema/komponent/person/adress/deladeadresselement-1.1" xmlns:ns4="http://statenspersonadressregister.se/schema/komponent/person/person-1.2" xmlns:ns5="http://statenspersonadressregister.se/schema/komponent/person/skyddadepersonuppgifter-1.1" xmlns:ns6="http://statenspersonadressregister.se/schema/komponent/person/inkomsttaxering-1.2" xmlns:ns7="http://statenspersonadressregister.se/schema/komponent/person/namn/deladenamnelement-1.0" xmlns:ns8="http://statenspersonadressregister.se/schema/komponent/person/namn/namn-1.0" xmlns:ns9="http://statenspersonadressregister.se/schema/komponent/person/avregistrering-1.0" xmlns:ns10="http://statenspersonadressregister.se/schema/komponent/person/persondetaljer-1.2" xmlns:ns11="http://statenspersonadressregister.se/schema/komponent/person/hanvisning-1.0" xmlns:ns12="http://statenspersonadressregister.se/schema/komponent/person/samordningsnummer-1.0" xmlns:ns13="http://statenspersonadressregister.se/schema/komponent/person/folkbokforing-1.0" xmlns:ns14="http://statenspersonadressregister.se/schema/komponent/person/adress/folkbokforingsadress-1.1" xmlns:ns15="http://statenspersonadressregister.se/schema/komponent/person/adress/sarskildpostadress-1.1" xmlns:ns16="http://statenspersonadressregister.se/schema/komponent/person/adress/utlandsadress-1.1" xmlns:ns17="http://statenspersonadressregister.se/schema/komponent/person/adress/kontaktadress-1.0" xmlns:ns18="http://statenspersonadressregister.se/schema/komponent/person/relation-1.2" xmlns:ns19="http://statenspersonadressregister.se/schema/komponent/person/fastighetstaxering-1.2" xmlns:ns20="http://statenspersonadressregister.se/schema/komponent/person/aviseringpost-1.2" xmlns:ns21="http://statenspersonadressregister.se/schema/komponent/sok/sokargument-1.2" xmlns:ns22="http://statenspersonadressregister.se/schema/komponent/sok/undantag-1.0" xmlns:ns23="http://statenspersonadressregister.se/schema/komponent/sok/personsokningsokparametrar-1.1" xmlns:ns24="http://statenspersonadressregister.se/schema/personsok/2021.1/personsokningfraga" xmlns:ns25="http://statenspersonadressregister.se/schema/personsok/2021.1/personsokningsvar">
            <ns23:PersonsokningFraga>
                <ns4:IdNummer>197901249297</ns4:IdNummer>
            </ns23:PersonsokningFraga>
            <ns25:PersonsokningSvarspost>
                <ns4:PersonId>
                    <ns4:IdNummer>197901249297</ns4:IdNummer>
                    <ns4:Typ>PERSONNUMMER</ns4:Typ>
                </ns4:PersonId>
                <ns5:Sekretessmarkering>NEJ</ns5:Sekretessmarkering>
                <ns5:SkyddadFolkbokforing>NEJ</ns5:SkyddadFolkbokforing>
                <ns4:SenasteAndringSPAR>2019-12-02</ns4:SenasteAndringSPAR>
                <ns8:Namn>
                    <ns2:DatumFrom>2019-12-02</ns2:DatumFrom>
                    <ns2:DatumTill>9999-12-31</ns2:DatumTill>
                    <ns7:Fornamn>Erik</ns7:Fornamn>
                    <ns7:Tilltalsnamn>10</ns7:Tilltalsnamn>
                    <ns7:Efternamn>Efternamn2009</ns7:Efternamn>
                </ns8:Namn>
                <ns10:Persondetaljer>
                    <ns2:DatumFrom>2019-12-02</ns2:DatumFrom>
                    <ns2:DatumTill>9999-12-31</ns2:DatumTill>
                    <ns5:Sekretessmarkering>NEJ</ns5:Sekretessmarkering>
                    <ns5:SkyddadFolkbokforing>NEJ</ns5:SkyddadFolkbokforing>
                    <ns10:Fodelsedatum>1979-01-24</ns10:Fodelsedatum>
                    <ns10:Kon>MAN</ns10:Kon>
                </ns10:Persondetaljer>
                <ns13:Folkbokforing>
                    <ns2:DatumFrom>2019-12-02</ns2:DatumFrom>
                    <ns2:DatumTill>9999-12-31</ns2:DatumTill>
                    <ns13:FolkbokfordLanKod>01</ns13:FolkbokfordLanKod>
                    <ns13:FolkbokfordKommunKod>80</ns13:FolkbokfordKommunKod>
                    <ns13:Hemvist>Skriven på adressen</ns13:Hemvist>
                    <ns13:Folkbokforingsdatum>1988-03-10</ns13:Folkbokforingsdatum>
                    <ns13:DistriktKod>215025</ns13:DistriktKod>
                </ns13:Folkbokforing>
                <ns13:Folkbokforing>
                    <ns2:DatumFrom>2010-02-02</ns2:DatumFrom>
                    <ns2:DatumTill>2019-12-02</ns2:DatumTill>
                    <ns13:FolkbokfordLanKod>01</ns13:FolkbokfordLanKod>
                    <ns13:FolkbokfordKommunKod>80</ns13:FolkbokfordKommunKod>
                    <ns13:Hemvist>Skriven på adressen</ns13:Hemvist>
                    <ns13:Folkbokforingsdatum>1988-03-10</ns13:Folkbokforingsdatum>
                </ns13:Folkbokforing>
                <ns14:Folkbokforingsadress>
                    <ns3:SvenskAdress>
                        <ns2:DatumFrom>2019-12-02</ns2:DatumFrom>
                        <ns2:DatumTill>9999-12-31</ns2:DatumTill>
                        <ns3:Utdelningsadress2>Gatan307 2</ns3:Utdelningsadress2>
                        <ns3:PostNr>11541</ns3:PostNr>
                        <ns3:Postort>STOCKHOLM</ns3:Postort>
                    </ns3:SvenskAdress>
                </ns14:Folkbokforingsadress>
                <ns15:SarskildPostadress>
                    <ns3:SvenskAdress>
                        <ns2:DatumFrom>2019-12-02</ns2:DatumFrom>
                        <ns2:DatumTill>9999-12-31</ns2:DatumTill>
                        <ns3:Utdelningsadress1>2241</ns3:Utdelningsadress1>
                        <ns3:Utdelningsadress2>Vägen12</ns3:Utdelningsadress2>
                        <ns3:PostNr>17071</ns3:PostNr>
                        <ns3:Postort>SOLNA</ns3:Postort>
                    </ns3:SvenskAdress>
                </ns15:SarskildPostadress>
                <ns15:SarskildPostadress>
                    <ns3:SvenskAdress>
                        <ns2:DatumFrom>2010-02-02</ns2:DatumFrom>
                        <ns2:DatumTill>2019-12-02</ns2:DatumTill>
                        <ns3:CareOf>CO-NAMN</ns3:CareOf>
                        <ns3:Utdelningsadress2>Gatan356 20</ns3:Utdelningsadress2>
                        <ns3:PostNr>14341</ns3:PostNr>
                        <ns3:Postort>VÅRBY</ns3:Postort>
                    </ns3:SvenskAdress>
                </ns15:SarskildPostadress>
            </ns25:PersonsokningSvarspost>
            <ns25:UUID>207799ca-0b54-4028-a97c-9e1eb1818a8f</ns25:UUID>
        </ns25:SPARPersonsokningSvar>
    </S:Body>
</S:Envelope>`;

export const sparPersonSokResponse = {
  PersonId: {
    IdNummer: "197901249297",
    Typ: "PERSONNUMMER",
  },
  Sekretessmarkering: "NEJ",
  SkyddadFolkbokforing: "NEJ",
  SenasteAndringSPAR: "2019-12-02",
  Namn: {
    DatumFrom: "2019-12-02",
    DatumTill: "9999-12-31",
    Fornamn: "Erik",
    Tilltalsnamn: "10",
    Efternamn: "Efternamn2009",
  },
  Persondetaljer: {
    DatumFrom: "2019-12-02",
    DatumTill: "9999-12-31",
    Sekretessmarkering: "NEJ",
    SkyddadFolkbokforing: "NEJ",
    Fodelsedatum: "1979-01-24",
    Kon: "MAN",
  },
  Folkbokforing: [
    {
      DatumFrom: "2019-12-02",
      DatumTill: "9999-12-31",
      FolkbokfordLanKod: "01",
      FolkbokfordKommunKod: "80",
      Hemvist: "Skriven på adressen",
      Folkbokforingsdatum: "1988-03-10",
      DistriktKod: "215025",
    },
    {
      DatumFrom: "2010-02-02",
      DatumTill: "2019-12-02",
      FolkbokfordLanKod: "01",
      FolkbokfordKommunKod: "80",
      Hemvist: "Skriven på adressen",
      Folkbokforingsdatum: "1988-03-10",
    },
  ],
  Folkbokforingsadress: {
    SvenskAdress: {
      DatumFrom: "2019-12-02",
      DatumTill: "9999-12-31",
      Utdelningsadress2: "Gatan307 2",
      PostNr: "11541",
      Postort: "STOCKHOLM",
    },
  },
  SarskildPostadress: [
    {
      SvenskAdress: {
        DatumFrom: "2019-12-02",
        DatumTill: "9999-12-31",
        Utdelningsadress1: "2241",
        Utdelningsadress2: "Vägen12",
        PostNr: "17071",
        Postort: "SOLNA",
      },
    },
    {
      SvenskAdress: {
        DatumFrom: "2010-02-02",
        DatumTill: "2019-12-02",
        CareOf: "CO-NAMN",
        Utdelningsadress2: "Gatan356 20",
        PostNr: "14341",
        Postort: "VÅRBY",
      },
    },
  ],
};

export const sparGraphQLResponse = {
  data: {
    getPerson: {
      birthdate: "1979-01-24",
      firstname: "Erik",
      lastname: "Efternamn2009",
    },
  },
};
