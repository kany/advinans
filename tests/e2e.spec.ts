import { config } from "dotenv";
import request from "supertest";
import nock from "nock";
import testApp from "../src/app";
import {
  sparWsdlResponse,
  sparPersonSokDto,
  sparPersonSokResponse,
  sparGraphQLResponse,
} from "./__fixtures";
import { print } from "graphql";
import gql from "graphql-tag";

config();

const mockWsdlResponses = () => {
  nock(process.env.SPAR_ENDPOINT_URL)
    .get("/?wsdl")
    .reply(200, sparWsdlResponse, { "Content-Type": "text/xml" });
  nock(process.env.SPAR_ENDPOINT_URL).post("/").reply(200, sparPersonSokDto);
};

describe("Test the health check endpoint", () => {
  test("it should respond with 200 on GET", async () => {
    const response = await request(testApp).get("/healthz");
    expect(response.statusCode).toBe(200);
  });
});

describe("Test spar endpoint", () => {
  test("it should respond with a registry post", async () => {
    mockWsdlResponses();
    const response = await request(testApp).get("/api/spar/197901249297/");
    expect(response.body).toEqual(sparPersonSokResponse);
    expect(response.statusCode).toBe(200);
  });

  test("it should respond with an error if the upstream server is unavailable", async () => {
    nock(process.env.SPAR_ENDPOINT_URL)
      .post("/?wsdl")
      .replyWithError({ code: "ECONNABORTED" });
    const response = await request(testApp).get("/api/spar/197901249297/");
    expect(response.text).toEqual("UPSTREAM_ERROR");
    expect(response.statusCode).toBe(500);
  });
});

describe("Test graphql endpoint", () => {
  test("it should respond with a GraphQL Person type", async () => {
    mockWsdlResponses();
    const query = print(gql`
      query {
        getPerson(ssn: "197901249297") {
          birthdate
          firstname
          lastname
        }
      }
    `);
    const response = await request(testApp).post("/graphql/").send({ query });

    expect(response.body).toEqual(sparGraphQLResponse);
    expect(response.statusCode).toBe(200);
  });
});

describe("Test unsupported endpoints", () => {
  test("it should respond with 404 for an unknown endpoint", async () => {
    const response = await request(testApp).get("/mock");
    expect(response.statusCode).toBe(404);
  });

  test("it should respond with 405 for an unsupported method", async () => {
    const response = await request(testApp).put("/api/");
    expect(response.statusCode).toBe(405);
  });
});
