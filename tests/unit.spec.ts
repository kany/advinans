import {
  validateFormat,
  validateDate,
  validateLuhn,
} from "../src/helpers/validators";

describe("Test ssn format validation", () => {
  test("it should be successful if correct format", async () => {
    const response1 = validateFormat("199900001111");
    const response2 = validateFormat("9900001111");
    const response3 = validateFormat("990000-1111");
    expect(response1).toEqual("199900001111");
    expect(response2).toEqual("199900001111");
    expect(response3).toEqual("199900001111");
  });

  test("it should fail if not 10 or 12 digits long", async () => {
    const wrapper1 = () => validateFormat("00000000000");
    const wrapper2 = () => validateFormat("0000000000000");
    expect(wrapper1).toThrowError("INVALID_SSN_FORMAT");
    expect(wrapper2).toThrowError("INVALID_SSN_FORMAT");
  });

  test("it should fail if it contains non-digits", async () => {
    const wrapper = () => validateFormat("000d000000");
    expect(wrapper).toThrowError("INVALID_SSN_FORMAT");
  });
});

describe("Test ssn date validation", () => {
  test("it should pass if valid", async () => {
    const wrapper = () => validateDate("19990101");
    expect(wrapper).not.toThrowError("INVALID_SSN_DATE");
  });

  test("it should fail if more than 100 years ago", async () => {
    const wrapper = () => validateDate("18990101");
    expect(wrapper).toThrowError("INVALID_SSN_DATE");
  });

  test("it should fail if in the future", async () => {
    const wrapper = () => validateDate("18990101");
    expect(wrapper).toThrowError("INVALID_SSN_DATE");
  });

  test("it should fail if not a valid date", async () => {
    const wrapper = () => validateDate("200021011");
    expect(wrapper).toThrowError("INVALID_SSN_DATE");
  });
});

describe("Test ssn luhn validation", () => {
  test("it should be successful if correct", async () => {
    const response = validateLuhn("7904050296");
    expect(response).toBe(true);
  });

  test("it should fail if incorrect", async () => {
    const wrapper = () => validateLuhn("7904050291");
    expect(wrapper).toThrowError("INVALID_SSN_LUHN");
  });
});
