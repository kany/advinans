# Installer
FROM node:18-alpine as installer
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install

# Runner
FROM node:18-alpine as runner
WORKDIR /app

COPY . .
COPY --from=installer /app/node_modules ./node_modules

ARG PORT
ARG SPAR_ENDPOINT_URL
ARG NODE_ENV=production

ENV SPAR_ENDPOINT_URL=$SPAR_ENDPOINT_URL
ENV NODE_ENV=$NODE_ENV

EXPOSE 8080
CMD yarn start
