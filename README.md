# README

### What is this repository for?

Advinans Code test & review

### Prerequisits

- Node version 18 or above
- Yarn package manager
- Postman or simillar
- SSL Certificate for Spar

### How do I get set up?

- Download the repo
- run `yarn`
- Copy your cert files (`bolag-a.crt` & `bolag-a.key`) to the cert folder

### How do I start develop mode?

- run `yarn dev`

### How do I start production mode?

- run `yarn start`

or

- run `yarn docker:build`
- run `yarn docker:run`

### How do I use it?

- Health check: GET http://localhost:8080/healthz
- Rest API: GET http://localhost:8080/api/spar/{ssn} (E.G 197901249297)
- GraphQL API: http://localhost:8080/graphql

### How do I test it?

- run `yarn test`
