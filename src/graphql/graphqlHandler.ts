import {
  graphql,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLBoolean,
} from "graphql";
import { validateSsn } from "@/helpers/validators";
import { getPersonBySsn } from "@/spar/sparApi";

const PersonType = new GraphQLObjectType({
  name: "Person",
  fields: {
    ssn: { type: GraphQLID },
    birthdate: { type: GraphQLString },
    gender: { type: GraphQLString },
    classified: { type: GraphQLBoolean },
    protected: { type: GraphQLBoolean },
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString },
    street: { type: GraphQLString },
    postalCode: { type: GraphQLString },
    postalArea: { type: GraphQLString },
  },
});

const query = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    getPerson: {
      type: PersonType,
      args: {
        ssn: { type: GraphQLID },
      },
      async resolve(parent, args) {
        const { ssn } = args;
        const validatedSsn = validateSsn(ssn);
        const response = await getPersonBySsn(validatedSsn);

        return {
          ssn: response.PersonId.IdNummer,
          birthdate: response.Persondetaljer.Fodelsedatum,
          gender: response.Persondetaljer.Kon,
          classified: response.Sekretessmarkering === "JA",
          protectedAddress: response.SkyddadFolkbokforing === "JA",
          firstname: response.Namn.Fornamn,
          lastname: response.Namn.Efternamn,
          street: response.Folkbokforingsadress.SvenskAdress.Utdelningsadress2,
          postalCode: response.Folkbokforingsadress.SvenskAdress.PostNr,
          postalArea: response.Folkbokforingsadress.SvenskAdress.Postort,
        };
      },
    },
  },
});

const schema = new GraphQLSchema({ query });

const graphqlHandler = (source: any) => graphql({ schema, source });

export default graphqlHandler;
