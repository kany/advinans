import express from "express";
import errorHandler from "@/helpers/errorHandler";
import graphqlHandler from "@/graphql/graphqlHandler";

const router = express.Router();

router.post("/", async (req, res) => {
  const { query } = req.body;

  try {
    const response = await graphqlHandler(query);

    if (response.errors) {
      res.status(400);
    }
    res.json(response);
  } catch (ex) {
    errorHandler(res, ex);
  }
});

export default router;
