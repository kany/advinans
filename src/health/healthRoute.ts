import express from "express";
import consola from "consola";

const router = express.Router();

router.get("*", async (req, res) => {
  try {
    res.send("ok");
  } catch (ex) {
    consola.error(ex);
  }
});

export default router;
