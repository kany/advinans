export type PersonSokResponse = [
  {
    PersonsokningFraga: {
      IdNummer: string;
    };
    PersonsokningSvarspost: {
      PersonId: {
        IdNummer: string;
        Typ: string;
      };
      Sekretessmarkering: string;
      SkyddadFolkbokforing: string;
      SenasteAndringSPAR: string;
      Namn: {
        DatumFrom: string;
        DatumTill: string;
        Fornamn: string;
        Tilltalsnamn: string;
        Efternamn: string;
      };
      Persondetaljer: {
        DatumFrom: string;
        DatumTill: string;
        Sekretessmarkering: string;
        SkyddadFolkbokforing: string;
        Fodelsedatum: string;
        Kon: string;
      };
      Folkbokforing: [
        {
          DatumFrom: string;
          DatumTill: string;
          FolkbokfordLanKod: string;
          FolkbokfordKommunKod: string;
          Hemvist: string;
          Folkbokforingsdatum: string;
          DistriktKod: string;
        },
        {
          DatumFrom: string;
          DatumTill: string;
          FolkbokfordLanKod: string;
          FolkbokfordKommunKod: string;
          Hemvist: string;
          Folkbokforingsdatum: string;
        }
      ];
      Folkbokforingsadress: {
        SvenskAdress: {
          DatumFrom: string;
          DatumTill: string;
          Utdelningsadress2: string;
          PostNr: string;
          Postort: string;
        };
      };
      SarskildPostadress: [
        {
          SvenskAdress: {
            DatumFrom: string;
            DatumTill: string;
            Utdelningsadress1: string;
            Utdelningsadress2: string;
            PostNr: string;
            Postort: string;
          };
        },
        {
          SvenskAdress: {
            DatumFrom: string;
            DatumTill: string;
            CareOf: string;
            Utdelningsadress2: string;
            PostNr: string;
            Postort: string;
          };
        }
      ];
    };
    UUID: string;
  },
  string,
  null,
  string,
  null
];
