import https from "https";
import * as soap from "soap";
import { cert, key } from "@/helpers/getCertificates";
import { PersonSokResponse } from "./sparApi.d";

const createSoapClient = async () => {
  const options = {
    escapeXML: false,
    wsdl_options: {
      httpsAgent: new https.Agent({ cert, key }),
    },
  };
  const client = await soap.createClientAsync(
    `${process.env.SPAR_ENDPOINT_URL}/?wsdl`,
    options
  );
  client.setSecurity(new soap.ClientSSLSecurity(key, cert));
  client.setEndpoint(`${process.env.SPAR_ENDPOINT_URL}/`);

  return client;
};

export const getPersonBySsn = async (ssn: string) => {
  const client = await createSoapClient();
  const query = {
    Identifieringsinformation: {
      KundNrLeveransMottagare: 500243,
      KundNrSlutkund: 500243,
      UppdragId: 637,
      SlutAnvandarId: "spar-lookup",
    },
    PersonsokningFraga: { IdNummer: ssn },
  };
  const response = <PersonSokResponse>await client.PersonSokAsync(query);
  return response[0].PersonsokningSvarspost;
};
