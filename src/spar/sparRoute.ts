import express from "express";
import { validateSsn } from "@/helpers/validators";
import { getPersonBySsn } from "@/spar/sparApi";
import errorHandler from "@/helpers/errorHandler";

const router = express.Router();

router.get("/spar/:ssn/", async (req, res) => {
  const { ssn } = req.params;
  let validatedSsn = "";

  try {
    validatedSsn = validateSsn(ssn);
  } catch (ex) {
    res.status(400).send(ex.message);
    return;
  }

  try {
    const data = await getPersonBySsn(validatedSsn);
    res.json(data);
  } catch (ex) {
    errorHandler(res, ex);
  }
});

router.all("*", (req, res) => {
  res.setHeader("Allow", ["GET"]);
  res.status(405).end(`METHOD_NOT_ALLOWED`);
});

export default router;
