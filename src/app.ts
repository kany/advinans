import express from "express";
import healthRoute from "@/health/healthRoute";
import sparRoute from "@/spar/sparRoute";
import graphqlRoute from "@/graphql/graphqlRoute";

const app = express();
app.use(express.json());

app.use("/healthz", healthRoute);
app.use("/api", sparRoute);
app.use("/graphql", graphqlRoute);
app.use("/favicon.ico", (req, res) => {
  res.status(204).end();
});

app.all("*", (req, res) => {
  res.status(404).send(`NOT_FOUND`);
});

export default app;
