import { Response } from "express";
import { isAxiosError } from "axios";
import consola from "consola";

const errorHandler = (res: Response, ex: any) => {
  if (process.env.NODE_ENV === "production") consola.error(ex);

  if (isAxiosError(ex)) {
    res.status(500);
    switch (ex.code) {
      case "ECONNABORTED":
        res.send("UPSTREAM_UNAVAILABLE");
        break;
      case "404":
      case "405":
        res.send("UPSTREAM_NOT_FOUND");
        break;
      case "500":
      default:
        res.send("UPSTREAM_ERROR");
        break;
    }
    return;
  }

  switch (ex.toString()) {
    case "Error: 404":
      res.status(404).send("NO_MATCH");
      break;
    case "Error: 500":
    default:
      res.status(500).send("SERVER_ERROR");
  }
};

export default errorHandler;
