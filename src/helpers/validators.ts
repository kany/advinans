export const validateFormat = (ssn: string) => {
  const regexPattern = /^(?:\d{6}-\d{4}|\d{10}|\d{12})$/;

  if (!regexPattern.test(ssn)) {
    throw new Error("INVALID_SSN_FORMAT");
  }

  let digitsOnly = ssn.replace("-", "");

  if (isNaN(Number(digitsOnly))) {
    throw new Error("INVALID_SSN_FORMAT");
  }

  if (digitsOnly.length === 10) {
    const currentYear = new Date().getFullYear();
    const firstTwoDigits = digitsOnly.slice(0, 2);
    const prefix =
      parseInt(firstTwoDigits, 10) > currentYear - 2000 ? "19" : "20";

    digitsOnly = prefix + digitsOnly;
  }

  return digitsOnly;
};

export const validateDate = (dateString: string) => {
  const year = parseInt(dateString.substring(0, 4));
  const month = parseInt(dateString.substring(4, 6)) - 1;
  const day = parseInt(dateString.substring(6, 8));

  const ssnDate = new Date(year, month, day);
  const minDate = new Date();
  const maxDate = new Date();

  minDate.setFullYear(maxDate.getFullYear() - 100);

  if (
    ssnDate < minDate ||
    ssnDate > maxDate ||
    month > 11 ||
    day < 1 ||
    day > 31
  ) {
    throw new Error("INVALID_SSN_DATE");
  }
};

export const validateLuhn = (input: string) => {
  const numdigits = input.length;
  const parity = numdigits % 2;
  let sum = 0;

  for (let i = 0; i < numdigits; i++) {
    let digit = parseInt(input.charAt(i));
    if (i % 2 === parity) digit *= 2;
    if (digit > 9) digit -= 9;
    sum += digit;
  }

  if (sum % 10 !== 0) {
    throw new Error("INVALID_SSN_LUHN");
  }

  return true;
};

export const validateSsn = (ssn: string) => {
  const formattedSsn = validateFormat(ssn);
  validateDate(formattedSsn.substring(0, 8));
  validateLuhn(formattedSsn.substring(2));
  return formattedSsn;
};
